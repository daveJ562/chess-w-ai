# Chess w Ai

```
**Install**
Pygame
sys
```

To run game, make sure you are in **main.py** file then hit run file

## Tuesday May 30,2023

Imported the images of the pieces in **Assets** Directory

Created In SRC

- **piece.py** file will give each piece a value ranking each piece based on importance. I also imported images of the pieces and assigned them to corresponding piece and color
- **dragger.py** is what will alow me to move pieces via cordinates using rows \* cols

### Commits

```
"Started the process of being able to move pieces"
```

## Monday May 29,2023

Created a SRC directory for the necessary files to create the game
Created In SRC

- **const.py** has the dimensions for the screen display
  and the amount of rows and columns
- **main.py** is the file that will be running our game (render our board)
- **game.py** will display our grid
- **board.py** will convert each square on the board into an object

### Commits

```
"Created the window display for the game"

"created the game.py file and inside that file I created a Game class with the show_bg method to display a light/dark green grid. The colors are currently hard coded but I will add a feature where user will have different options to choose from"


```
