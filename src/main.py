import pygame
#  Python modules designed
# for writing video games. It provides functionality for handling graphics, input,
# and other game-related operations.
import sys
#  provides access to some variables used or maintained by the interpreter and
#  functions that interact with the interpreter

from const import WIDTH, HEIGHT, ROWS, COLS, SQSIZE
from game import Game

class Main:
    def __init__(self):
        pygame.init()
        self.screen = pygame.display.set_mode( (WIDTH, HEIGHT) )
        pygame.display.set_caption("Chess")
        self.game = Game()


    def mainloop(self):

        game = self.game
        screen = self.screen

        while True:

            game.show_bg(screen)
            game.show_pieces(screen)
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    pass

                elif event.type == pygame.MOUSEMOTION:
                    pass

                elif event.type == pygame.MOUSEBUTTONUP:
                    pass

                elif event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()


            pygame.display.update()




main = Main()
main.mainloop()
