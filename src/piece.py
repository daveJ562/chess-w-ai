import os


class Piece:

    def __init__(self, name, color, value, texture=None, texture_rect=None):
        self.name = name
        self.color = color
        if color == "white":
            value_sign = 1
        else:
            value_sign = -1
        self.value = value * value_sign
        self.moves = []
        self.moved = False
        self.texture = texture
        self.set_texture()
        self.texture_rect = texture_rect

    def set_texture(self, size=80):
        self.texture = os.path.join(
            f'assets/images/imgs-{size}px/{self.color}_{self.name}.png'
        )

    def add_moves(self, move):
        self.moves.append(move)


class Pawn(Piece):

    def __init__(self, color):
        if color == "white":
            self.dir = -1
            # we use -1 here bc the white pieces start at the bottom
            # of the y-axis and only move forward which is up
            # * AXIS starts at top left of window
        else:
            self.dir = 1
        super().__init__("pawn", color, 1.0)
        # value at the end will tell ai how important that piece is


class Knight(Piece):
    def __init__(self, color):
        super().__init__("knight", color, 3.0)


class Bishop(Piece):
    def __init__(self, color):
        super().__init__("bishop", color, 3.001)


class Rook(Piece):
    def __init__(self, color):
        super().__init__("rook", color, 5.0)


class Queen(Piece):
    def __init__(self, color):
        super().__init__("queen", color, 9.0)


class King(Piece):
    def __init__(self, color):
        super().__init__("king", color, 1000.0)
        # we give this a high value to tell ai not to lose this piece
